# Configuration de deux serveurs DHCPv4 en HA avec Kea et d'une base de données

## Prérequis

Pour notre installation on va utiliser deux machines sous RedHat qui serviront de serveurs DHCP, et d'une machine sous Debian 11 (Bullseye) qui servira de base de données MariaDB.

dhcp1 : 10.16.33.32  
dhcp2 : 10.16.33.84  
db1 : 10.16.33.156

## Installation

### Installation de Kea sur les deux serveurs DHCP
---
Pour l'installation j'ai utilisé les paquets natifs de kea disponibles [ici](https://cloudsmith.io/~isc/repos/kea-2-0/packages/).

Ensuite on met à jour les dépôts puis on installe tous les paquets:
```
$ yum update
$ yum install isc-kea*
```
On peut vérifier que les fichiers de configurations se situent bien dans le répertoire */etc/kea*.

### Installation de la base de données MariaDB
---
Installation de MariaDB sur la machine Debian:
```
$ apt install mariadb-server-10.5
$ mysql_secure_installation
```
On ajoute un mot de passe à l'utilisateur __root__ et c'est terminé.

## Configuration de la base de données

### Création d'une base et d'un utilisateur
---
Pour commencer on crée une nouvelle base de données qui sera utilisée par les serveurs DHCP:
```
mysql> CREATE DATABASE kea;
```

Puis on va ajouter un utilisateur et lui attribuer tous les droits sur cette base:
```
mysql> CREATE USER 'kea-dhcp'@'localhost' IDENTIFIED BY 'superpassword123';
mysql> GRANT ALL ON kea.* TO 'kea-dhcp'@'localhost';
```

### Attribution des droits pour les serveurs DHCP
---
Pour donner l'accès à cette base à nos serveurs il faut commenter cette ligne dans le fichier de configuration */etc/mysql/mariadb.conf.d/50-server.cnf* :
```
bind-address = 127.0.0.1
```

Et il faut ensuite exécuter ces trois commandes sur la base de données:
```
mysql> GRANT ALL ON kea.* TO 'kea-dhcp'@'10.16.33.32';
mysql> GRANT ALL ON kea.* TO 'kea-dhcp'@'10.16.33.84';
mysql> FLUSH PRIVILEGES;
```

Maintenant nos serveurs DHCP peuvent se connecter à distance sur cette base de données.

### Ajout des tables dans la base de données
---
Pour ajouter les tables dans la base de données, on va utiliser l'outil __kea-admin__ qui est installé sur nos deux serveurs DHCP:
```
$ kea-admin db-init mysql -h 10.16.33.156 -u kea-dhcp -n kea -p
```

On vérifie que nos tables sont bien présentes dans la base et on passe à la configuration des serveurs.

## Configuration des serveurs DHCPv4

### Configuration de base
---
La configuration d'un serveur s'effectue dans le fichier */etc/kea/kea-dhcp4.conf* . On va commencer par lui spécifier l'interface sur laquelle le serveur doit écouter pour recevoir les baux DHCP:
```
"interfaces-config": {
    "interfaces": ["eth0"]
},
```

On spécifie ensuite les paramètres pour la durée d'un bail, son renouvellement... (plus d'infos sur la [doc](https://kea.readthedocs.io/en/latest/arm/lease-expiration.html#lease-expiration))
```
"valid-lifetime": 600,
"renew-timer": 150,
"rebind-timer": 300,
```

On peut rajouter des options comme le fait de délivrer l'adresse ip du DNS à tous les clients DHCP. (plus d'infos sur la [doc](https://kea.readthedocs.io/en/latest/arm/dhcp4-srv.html#standard-dhcpv4-options))
```
"option-data": [
{
    "name": "domain-name-servers",
    "data": "195.220.223.2",
    "always-send": true
}
],
```

Passons maintenant à l'ajout des subnets, pour cela on spécifie l'adresse de réseau et le masque du subnet en question, puis on lui indique un pool d'adresses IP, et enfin on peut lui ajouter des options comme le fait d'attribuer aux clients de ce subnet l'adresse ip du routeur. (plus d'infos sur la [doc](https://kea.readthedocs.io/en/latest/arm/dhcp4-srv.html#configuration-of-ipv4-address-pools))
```
"subnet4": [
{
    "subnet": "10.0.0.0/24",
    "pools": [ { "pool": "10.0.0.1 - 10.0.0.250" } ],
    "option-data": [
        {
            "name": "routers",
            "data": "10.0.0.254"
        }
    ]
}
],
```

On peut spécifier l'endroit où seront stockées les baux DHCP, en l'occurrence sur notre base de données que nous avons configurées précédemment.
```
"lease-database": {
    "type": "mysql",
    "name": "kea",
    "host": "10.16.33.156",
    "connect-timeout": 15,
    "max-reconnect-tries": 5,
    "reconnect-wait-time": 5000,
    "user": "kea-dhcp",
    "password": "superpassword123"
},
```

`connect-timeout` :  Indique le temps en secondes au bout duquel la base de donnée est considérée comme injoignable.

`max-reconnect-tries` : Nombre de reconnexions max à la base de données en cas d'erreur.

`reconnect-wait-time` : Temps d'attente en millisecondes entre les tentatives de reconnexions. (plus d'infos sur la [doc](https://kea.readthedocs.io/en/latest/arm/dhcp4-srv.html#lease-storage))

---

Possibilité d'ajouter un fichier de logs et d'ajuster le paramètre __severity__ pour plus ou moins de détails. (plus d'infos sur la [doc](https://kea.readthedocs.io/en/latest/arm/logging.html#the-severity-string-logger))
```
"loggers": [
{
    "name": "kea-dhcp4",
    "output_options": [
        {
            "output": "/var/log/kea/dhcp4.log"
        }
    ],
    "severity": "INFO"
} 
]
```

### Configuration de la réservation d'hôtes dans la base de données
---
Pour la réservations d'hôtes, on spécifie la base de données à contacter dans le fichier */etc/kea/kea-dhcp4.conf* . (plus d'infos sur la [doc](https://kea.readthedocs.io/en/latest/arm/dhcp4-srv.html#hosts-storage))
```
"hosts-database": {
    "type": "mysql",
    "name": "kea",
    "host": "10.16.33.156",
    "connect-timeout": 15,
    "max-reconnect-tries": 5,
    "reconnect-wait-time": 5000,
    "user": "kea-dhcp",
    "password": "superpassword123"
},
```

Et maintenant pour peupler la base de données, nous allons utiliser le script [host_rsv_mysql.txt](./host_rsv_mysql.txt) sur lequel on peut changer les valeurs suivantes en fonction de nos besoin:
```
SET @ipv4_reservation='10.0.0.25';
SET @identifier_value='8c:47:be:53:84:ba';
```

Une façon plus simple de peupler la base serait d'utiliser la librairie __libdhcp_hosts_cmds.so__ mais elle est payante.

### Configuration du Load-Balancing
---
Le Load-Balancing va permettre de répartir la charge de travail entre nos deux serveurs DHCP. Pour cela il utilise un socket, des librairies et le __kea-ctrl-agent__ qu'on va configurer juste après. Pour commencer on va configurer, dans le fichier */etc/kea/kea-dhcp4.conf*  ,l'ouverture d'un socket qui va servir pour la communication avec l'API:
```
"control-socket": {
    "socket-type": "unix",
    "socket-name": "/tmp/kea4-ctrl-socket"
},
```

Ensuite on va configurer le load-balancing à l'aide de deux librairies qui sont *libdhcp_mysql_cb.so* et *libdhcp_ha.so* . Cette dernière va prendre en paramètres différentes choses comme le nom du serveur, le mode ou encore les peers connectés. (plus d'infos sur la [doc](https://kea.readthedocs.io/en/latest/arm/hooks.html#load-balancing-configuration))
```
"hooks-libraries": [{
    "library": "/usr/lib64/kea/hooks/libdhcp_mysql_cb.so"
}, {
    "library": "/usr/lib64/kea/hooks/libdhcp_ha.so",
    "parameters": {
        "high-availability": [{
            "this-server-name": "dhcp-adm-1",
            "mode": "load-balancing",
            "max-response-delay": 20000,
            "max-unacked-clients": 1,
            "send-lease-updates": false,
            "sync-leases": false,
            "peers": [{
                "name": "dhcp-adm-1",
                "url": "http://10.16.33.32:8000/",
                "role": "primary",
                "auto-failover": true
            }, {
                "name": "dhcp-adm-2",
                "url": "http://10.16.33.84:8000/",
                "role": "secondary",
                "auto-failover": true
            }]
        }]
    }
}],
```
Le load-balancing fonctionne sur des envois de "heartbeat" transitant par le control channel (spécifié par le paramètre `url`). Si un serveur ne reçoit plus de heartbeat de la part du deuxième serveur, il passe en *communication-recovery*. Si au bout d'un certain temps ou en fonction des paramètres spécifiés la communication n'est pas rétablie avec l'autre serveur, alors ce serveur passe en mode "partner-down" et commence à servir les clients DHCP du serveur qui ne répond plus.

`max-response-delay` : Le temps de réponse max d'un heartbeat avant que le serveur passe en mode "partner-down".

`max-unacked-clients` : Spécifie le nombre de clients max n'ayant pas reçu de "ack" avant de passer en mode "partner-down".

`send-lease-updates`, `sync-leases` : Synchronisation des baux DHCP avec les peers. (Dans notre cas la valeur est sur __false__ car les baux sont stockés sur la base de données)

`peers` : Déclaration des peers pour le load-balancing

`auto-failover` : Pour servir automatiquement les clients DHCP de l'autre serveur s'il ne répond plus

On peut aussi établir une connexion sécurisée à l'aide de TLS/HTTPS. (plus d'infos sur la [doc](https://kea.readthedocs.io/en/latest/arm/security.html#tls-https-support))

---

Après avoir fini la configuration dans le fichier */etc/kea/kea-dhcp4.conf*  , nous allons devoir configurer le __control-agent__ dans le fichier [*/etc/kea/kea-ctrl-agent.conf*](./kea-ctrl-agent.conf) .

On va spécifier dans ce fichier l'adresse IP et le port d'écoute du serveur DHCP, ainsi que le socket pour l'envoi et la réception à travers le control channel. (plus d'infos sur la [doc](https://kea.readthedocs.io/en/latest/arm/hooks.html#control-agent-configuration))
```
"Control-agent": {
    "http-host": "10.16.33.32",
    "http-port": 8000,

    "control-sockets": {
	"dhcp4": {
            "socket-type": "unix",
            "socket-name": "/tmp/kea4-ctrl-socket"
	}
    },
}
```

## Test du fonctionnement de l'API

Pour tester le fonctionnement de l'API RESTful on va ajouter une librairie dans le fichier */etc/kea/kea-dhcp4.conf*  , qui va nous permettre, par le biais de la commande __curl__, de récupérer des baux dans la base de données.

Pour cela on ajoute la librairie __libdhcp_lease_cmds.so__  :
```
"hooks-libraries": [{
    "library": "/usr/lib64/kea/hooks/libdhcp_lease_cmds.so"
},
...
```

On va ensuite créer un fichier JSON pour récupérer tous les baux présents dans la base de données. La liste des commandes disponibles avec la librairie ajoutée précédemment se situe dans leur [documentation](https://kea.readthedocs.io/en/latest/arm/hooks.html#lease-cmds-lease-commands).

Voici le fichier [param.json](./param.json) que j'ai crée:
```
{
    "command": "lease4-get-all",
    "service": [ "dhcp4" ]
}
```

`command` : Spécifie une commande présente dans la documentation

`service` : Désigne le type de serveur à qui il faut envoyer la requête (dans notre cas un serveur DHCPv4)

Pour envoyer la requête nous allons utiliser la commande __curl__ (plus d'infos sur les requêtes dans leur [doc](https://kea.readthedocs.io/en/latest/arm/ctrl-channel.html#management-api)).
```
$ curl -X POST -H "Content-Type: application/json" -d @param.json http://10.16.33.84:8000/ | jq .
```
`jq` : Permet de formater la réponse pour la rendre plus lisible

## Stockage de la configuration dans la base de données

Pour stocker la configuration des serveurs dans la base de données il faut ajouter quelques lignes dans notre fichier */etc/kea/kea-dhcp4.conf*  :
```
"config-control": {
    "config-databases": [{
        "type": "mysql",
        "name": "kea",
        "host": "10.16.33.156",
        "connect-timeout": 15,
        "max-reconnect-tries": 5,
        "reconnect-wait-time": 5000,
        "user": "kea-dhcp",
        "password": "superpassword123"
    }],
    "config-fetch-wait-time": 20
},
```

`config-databases` : Configuration du backend avec le nom, l'IP et les identifiants de la base

`config-fetch-wait-time` : Intervalle de temps entre la deux récupérations de configuration dans la base


Pour remplir la base de données on doit utiliser la librairie __libdhcp_cb_cmds.so__ qui est payante aussi.

Voici un [lien](https://gitlab.isc.org/isc-projects/kea/-/wikis/designs/configuration%20in%20db%20design) pour mieux comprendre le fonctionnement du backend et de la récupération des configurations.

---

# Configuration d'un Galera cluster avec du Load-Balancing

## Choix du Load-Balancer

Nous avions le choix d'utiliser un load-balancer en combinaison avec un galera cluster, ou tout simplement d'utiliser un galera cluster et de renseigner les adresses IP de chaque base de données dans les fichiers de configuration des serveurs DHCP. Cette dernière solution permettrait de changer de base de données si celle qui est actuellement utilisée devenait défaillante, mais le fait de renseigner plusieurs IP de bases de données dans les fichiers de configuration n'est pas possible dans la version actuelle de KEA. Nous avons donc choisi la première solution qui est d'utiliser un load-balancer.

## Prérequis

Pour cette installation nous allons utiliser la base de données créée précédemment ainsi que deux autres machines Debian pour créer un cluster de trois bases de données. Nous avons aussi besoin d'une dernière machine Debian qui servira de load-balancer.

db1 : 10.16.33.156  
db2 : 10.16.33.18  
db3 : 10.16.33.56  
load-balancer : 10.16.33.111

## Configuration du galera cluster

Pour configurer le galera cluster nous allons devoir modifier le fichier */etc/mysql/mariadb.conf.d/60-galera.cnf* sur chaque machine (aussi appelé noeud), et y ajouter les lignes suivantes :
```
wsrep_on                 = ON
wsrep_provider           = /usr/lib/galera/libgalera_smm.so
wsrep_cluster_name       = "galera-cluster"
wsrep_cluster_address    = "gcomm://10.16.33.156,10.16.33.18,10.16.33.56"
wsrep_node_address       = "10.16.33.156"
wsrep_node_name          = db1
binlog_format            = row
default_storage_engine   = InnoDB
innodb_autoinc_lock_mode = 2
bind-address = 0.0.0.0
```
Les seules valeurs à changer entre les noeuds sont __wsrep_node_address__ et __wsrep_node_name__ .

Pour initialiser le nouveau cluster il va falloir arrêter le service mariadb sur les trois noeuds et démarrer le premier noeud (peu importe lequel des trois), avec la commande suivante:
```
$ galera_new_cluster
```

Pour vérifier que le cluster est bien opérationnel et qu'il comporte un noeud on peut utiliser cette commande :
```
$ mysql -u root -p -e "SHOW STATUS LIKE 'wsrep_cluster_size';"
```

On peut à présent démarrer le service mariadb sur les deux noeuds restants et vérifié l'état du cluster :
```
$ mysql -u root -p -e "SHOW STATUS LIKE 'wsrep_%';"
```

La base de données créée précédemment devrait être répliquée sur les deux autres noeuds du cluster.

## Configuration de galera load-balancer

Après avoir installé [galera load-balancer](https://galeracluster.com/library/documentation/glb.html), on va modifier le fichier */etc/default/glbd* pour y ajouter les adresses des noeuds du cluster, le port d'écoute pour les connexions entrantes des clients, et l'adresse pour contrôler les connexions.
```
LISTEN_ADDR="3306"
CONTROL_ADDR="10.16.33.111:4444"
DEFAULT_TARGETS="10.16.33.156:3306 10.16.33.18:3306 10.16.33.56:3306"
```

Il nous suffit alors de redémarrer le service __glb__ et de vérifier que les noeuds sont bien présents dans la table.
```
$ systemctl daemon-reload
$ service glb restart
$ service glb getinfo
```

### Ajout des droits dans la base de données
---
On ajoute les droits de connexion à la base pour l'utilisateur __kea-dhcp__ depuis l'adresse IP __10.16.33.111__ .
```
mysql> GRANT ALL ON kea.* TO 'kea-dhcp'@'10.16.33.111' identified by 'superpassword123';
```

### Modification de la configuration des serveurs DHCPv4
---
Pour pouvoir joindre le load-balancer, il va falloir modifier l'adresse IP à contacter dans le fichier */etc/kea/kea-dhcp4.conf* .
```
"config-control": {
    "config-databases": [{
        "type": "mysql",
        "name": "kea",
        "host": "10.16.33.111",
        "connect-timeout": 15,
        "max-reconnect-tries": 5,
        "reconnect-wait-time": 5000,
        "user": "kea-dhcp",
        "password": "superpassword123"
    }],
    "config-fetch-wait-time": 20
},

"lease-database": {
    "type": "mysql",
    "name": "kea",
    "host": "10.16.33.111",
    "connect-timeout": 15,
    "max-reconnect-tries": 5,
    "reconnect-wait-time": 5000,
    "user": "kea-dhcp",
    "password": "superpassword123"
},

"hosts-database": {
    "type": "mysql",
    "name": "kea",
    "host": "10.16.33.111",
    "connect-timeout": 15,
    "max-reconnect-tries": 5,
    "reconnect-wait-time": 5000,
    "user": "kea-dhcp",
    "password": "superpassword123"
},
```

On relance les serveurs DHCP et on vérifie le bon fonctionnement avec la commande suivante :
```
$ echo getinfo | nc -q 1 10.16.33.111 4444

Router:                                                                                                             
------------------------------------------------------
        Address       :   weight   usage    map  conns
   10.16.33.156:3306  :    1.000   0.667    N/A      2
    10.16.33.18:3306  :    1.000   0.667    N/A      2
    10.16.33.56:3306  :    1.000   0.667    N/A      2
------------------------------------------------------
Destinations: 3, total connections: 6 of 493 max
```
On peut voir qu'il y a un total de 6 connexions, c'est normal car il y a 3 connexions par serveur DHCP, une pour les hôtes, une pour les baux, et une dernière pour récupérer la configuration dans la base.
